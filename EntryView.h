//
//  EntryView.h
//  MacLedger
//
//  Created by Eric Betts on 8/10/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface EntryView : NSObject {
    NSMutableDictionary         *root;
}
@property (assign,readonly) NSMutableDictionary *root;
@end
