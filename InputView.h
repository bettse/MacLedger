//
//  InputView.h
//  MacLedger
//
//  Created by Eric Betts on 8/7/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface InputView : NSObject {
    IBOutlet NSTextField *entryInput;
    IBOutlet NSTextView *entryOutput;    
}

@property (nonatomic, retain) IBOutlet NSTextView *entryOutput;
@property (nonatomic, retain) IBOutlet NSTextField *entryInput;

- (IBAction)click:(id)sender;
- (IBAction)characterTyped:(id)sender;
@end
