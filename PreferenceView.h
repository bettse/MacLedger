//
//  PreferenceView.h
//  MacLedger
//
//  Created by Eric Betts on 8/9/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface PreferenceView : NSObject {
    IBOutlet NSTextField *ledgerFile;

}
@property (nonatomic, retain) IBOutlet NSTextField *ledgerFile;

- (IBAction)click:(id)sender;
- (void) selectFile;


@end
