//
//  PreferenceView.m
//  MacLedger
//
//  Created by Eric Betts on 8/9/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import "PreferenceView.h"


@implementation PreferenceView

@synthesize ledgerFile;

- (IBAction)click:(id)sender{
    [self selectFile];
}

- (void) selectFile
{
    // Create the File Open Dialog class.
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    
    // Enable the selection of files in the dialog.
    [openDlg setCanChooseFiles:YES];
    
    // Enable the selection of directories in the dialog.
    [openDlg setCanChooseDirectories:NO];
    
    // Display the dialog.  If the OK button was pressed,
    // process the files.
    if ( [openDlg runModalForDirectory:nil file:nil] == NSOKButton )
    {
        // Get an array containing the full filenames of all
        // files and directories selected.
        NSArray* files = [openDlg filenames];
        
        NSString* fileName = [files objectAtIndex:0];

        [ledgerFile setStringValue:fileName];
        [[[NSUserDefaultsController sharedUserDefaultsController] values] setValue:fileName forKey:@"ledgerPath"];

        

    }
    
    
}


@end
