//
//  MacLedgerAppDelegate.m
//  MacLedger
//
//  Created by Eric Betts on 7/31/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import "MacLedgerAppDelegate.h"

@implementation MacLedgerAppDelegate

@synthesize window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// Insert code here to initialize your application 
    cv = [[CommandView alloc] init];  
    [cv setDefaultFont];

    iv = [[InputView alloc] init];  
    
    pv = [[PreferenceView alloc] init];
    
    ev = [[EntryView alloc] init];

}

@end
