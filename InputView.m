//
//  InputView.m
//  MacLedger
//
//  Created by Eric Betts on 8/7/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import "InputView.h"


@implementation InputView
@synthesize entryOutput;
@synthesize entryInput;


- (void)controlTextDidChange:(NSNotification *)nd
{
    [self characterTyped:NULL];
    
    
}

- (IBAction)click:(id)sender{
    //Append entryOutput to ledger file
    NSFileHandle *file;
    NSData *data;
        
    data = [[entryOutput string] dataUsingEncoding:NSUTF8StringEncoding];
    
    
    file = [NSFileHandle fileHandleForUpdatingAtPath: [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"ledgerPath"]];
    
    if (file == nil)
        NSLog(@"Failed to open file");
    
    
    [file seekToEndOfFile];
    
    [file writeData: data];
    
    [file closeFile];
    
}

- (IBAction)characterTyped:(id)sender
{

    NSTask *task;
    NSArray *arguments;
    NSPipe *pipe;
    NSFileHandle *file;
    NSData *data;

    //NSLog (@"Running ledger with arguments: entry %@", [entryInput stringValue]);
    
    if(![@"" isEqualToString:[entryInput stringValue]]){
        arguments = [NSArray arrayWithObjects:@"-f", [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"ledgerPath"], @"entry", [entryInput stringValue],  nil];
        
        task = [[NSTask alloc] init];
        
        [task setLaunchPath: @"/usr/local/bin/ledger"];
        [task setArguments: arguments];
        
        pipe = [NSPipe pipe];
        [task setStandardOutput: pipe];
        [task setStandardError: pipe];
        
        file = [pipe fileHandleForReading];
        [task launch];
        [task waitUntilExit];
        
        data = [file readDataToEndOfFile];
        
        [entryOutput setString:[[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding]];
    }
}


@end
