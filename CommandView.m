//
//  CommandView.m
//  MacLedger
//
//  Created by Eric Betts on 8/6/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import "CommandView.h"


@implementation CommandView

@synthesize commandOutput;
@synthesize commandInput;

- (CommandView*) init
{
    self = [super init];    
    if(self){
        comboData = [[NSMutableArray arrayWithCapacity:5] retain];
        [comboData addObject:@"-c bal bank"];
        [comboData addObject:@"-w --sort d --tail 5 -c reg Checking"];
        [comboData addObject:@"-w --forecast 'd<=[today]+183' -d 'd>[today] & d<[today]+183' --sort d reg Checking"];
        
    }
    return self;
}

-(void)dealloc
{
    [comboData release];
    [super dealloc];
}


-(void) setDefaultFont{
    [[commandOutput textStorage] setFont:[NSFont fontWithName:@"Menlo" size:14]];

}

- (IBAction)click:(id)sender{
    [self executeCommand];
}

- (void) executeCommand{

    NSTask *task;
    NSMutableArray *arguments;
    NSPipe *pipe;
    NSFileHandle *file;
    NSData *data;
    NSRange DoubleQuote;
    NSRange SingleQuote;
    NSScanner *theScanner;
    NSString *temp;
    
    NSLog (@"Running ledger with arguments: %@", [commandInput stringValue]);
    [self addObject:[commandInput stringValue]];

    arguments = [NSMutableArray arrayWithObjects:@"-f", [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:@"ledgerPath"], nil];
    
    DoubleQuote = [[commandInput stringValue] rangeOfString:@"\""];
    SingleQuote = [[commandInput stringValue] rangeOfString:@"'"];
    if(DoubleQuote.location == NSNotFound && SingleQuote.location == NSNotFound){
        [arguments addObjectsFromArray:[[commandInput stringValue] componentsSeparatedByString:@" "]];
    }else{
        //use NSScanner to find quoted strings and add them as individual arguments
        theScanner = [NSScanner scannerWithString:[commandInput stringValue]];        
        while ([theScanner isAtEnd] == NO) {
            [theScanner scanUpToString:@" " intoString:&temp];
            [arguments addObject:temp];
        }
    }
    
    task = [[NSTask alloc] init];
    
    [task setLaunchPath: @"/usr/local/bin/ledger"];
    [task setArguments: arguments];
    
    pipe = [NSPipe pipe];
    [task setStandardOutput: pipe];
    [task setStandardError: pipe];
    
    file = [pipe fileHandleForReading];
    [task launch];
    [task waitUntilExit];

    data = [file readDataToEndOfFile];
    
    [commandOutput setString:[[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding]];
    
    
}

- (int)numberOfItemsInComboBox:(NSComboBox *)aComboBox
{
    return [comboData count];
}

- (id)comboBox:(NSComboBox *)aComboBox objectValueForItemAtIndex:(int)index
{
    return [comboData objectAtIndex:index];
}

- (unsigned int)comboBox:(NSComboBox *)aComboBox indexOfItemWithStringValue:(NSString *)string
{
    return [comboData indexOfObject:string];
}

- (void) addObject:(NSString*)string
{
    for (NSString *element in comboData) {
        if ([element compare:string])
            return;
    }
    [comboData addObject:[commandInput stringValue]];
   
}


@end
