//
//  main.m
//  MacLedger
//
//  Created by Eric Betts on 7/31/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
