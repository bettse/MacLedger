//
//  MacLedgerAppDelegate.h
//  MacLedger
//
//  Created by Eric Betts on 7/31/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "CommandView.h"
#import "InputView.h"
#import "PreferenceView.h"
#import "EntryView.h"

@interface MacLedgerAppDelegate : NSObject <NSApplicationDelegate> {
    NSWindow *window;
    CommandView *cv;
    InputView *iv;
    PreferenceView *pv;
    EntryView *ev;
}

@property (assign) IBOutlet NSWindow *window;

@end
