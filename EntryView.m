//
//  EntryView.m
//  MacLedger
//
//  Created by Eric Betts on 8/10/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import "EntryView.h"


@implementation EntryView

@synthesize root;

- (id)init
{
    if ((self = [super init]))
    {
        root = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [root release];
    [super dealloc];
}

- (int)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
    // The NSOutlineView calls this when it needs to know how many children
    // a particular item has. Because we are using a standard tree of NSDictionary,
    // NSArray, NSString etc objects, we can just return the count.
    
    // The root node is special; if the NSOutline view asks for the children of nil,
    // we give it the count of the root dictionary.
    
    if (item == nil)
    {
        return [root count];
    }
    
    // If it's an NSArray or NSDictionary, return the count.
    
    if ([item isKindOfClass:[NSDictionary class]] || [item isKindOfClass:[NSArray class]])
    {
        return [item count];
    }
    
    // It can't have children if it's not an NSDictionary or NSArray.
    
    return 0;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
    // NSOutlineView calls this when it needs to know if an item can be expanded.
    // In our case, if an item is an NSArray or NSDictionary AND their count is > 0
    // then they are expandable.
    
    if ([item isKindOfClass:[NSArray class]] || [item isKindOfClass:[NSDictionary class]])
    {
        if ([item count] > 0)
            return YES;
    }
    
    // Return NO in all other cases.
    
    return NO;
}

- (id)outlineView:(NSOutlineView *)outlineView child:(int)index ofItem:(id)item
{
    // NSOutlineView will iterate over every child of every item, recursively asking
    // for the entry at each index. We return the item at a given array index,
    // or at the given dictionary key index.
    
    if (item == nil)
    {
        item = root;
    }
    
    if ([item isKindOfClass:[NSArray class]])
    {
        return [item objectAtIndex:index];
    }
    else if ([item isKindOfClass:[NSDictionary class]])
    {
        return [item objectForKey:[[item allKeys] objectAtIndex:index]];
    }
    
    return nil;
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
    // NSOutlineView calls this for each column in your NSOutlineView, for each item.
    // You need to work out what you want displayed in each column; in our case we
    // create in Interface Builder two columns, one called "Key" and the other "Value".
    //
    // If the NSOutlineView is after the key for an item, we use either the NSDictionary
    // key for that item, or we count from 0 for NSArrays.
    //
    // Note that you can find the parent of a given item using [outlineView parentForItem:item];
    
    if ([[[tableColumn headerCell] stringValue] compare:@"Key"] == NSOrderedSame)
    {
        // Return the key for this item. First, get the parent array or dictionary.
        // If the parent is nil, then that must be root, so we'll get the root
        // dictionary.
        
        id parentObject = [outlineView parentForItem:item] ? [outlineView parentForItem:item] : root;
        
        if ([parentObject isKindOfClass:[NSDictionary class]])
        {
            // Dictionaries have keys, so we can return the key name. We'll assume
            // here that keys/objects have a one to one relationship.
            
            return [[parentObject allKeysForObject:item] objectAtIndex:0];
        }
        else if ([parentObject isKindOfClass:[NSArray class]])
        {
            // Arrays don't have keys (usually), so we have to use a name
            // based on the index of the object.
            
            return [NSString stringWithFormat:@"Item %d", [parentObject indexOfObject:item]];
        }
    }
    else
    {
        // Return the value for the key. If this is a string, just return that.
        
        if ([item isKindOfClass:[NSString class]])
        {
            return item;
        }
        else if ([item isKindOfClass:[NSDictionary class]])
        {
            return [NSString stringWithFormat:@"%d items", [item count]];
        }
        else if ([item isKindOfClass:[NSArray class]])
        {
            return [NSString stringWithFormat:@"%d items", [item count]];
        }
    }
    
    return nil;
}

@end
