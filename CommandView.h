//
//  CommandView.h
//  MacLedger
//
//  Created by Eric Betts on 8/6/10.
//  Copyright 2010 Betts inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface CommandView : NSObject <NSComboBoxDataSource> {
    IBOutlet NSTextView *commandOutput;    
    IBOutlet NSTextField *commandInput;
    NSMutableArray *comboData;
}

@property (nonatomic, retain) IBOutlet NSTextView *commandOutput;
@property (nonatomic, retain) IBOutlet NSTextField *commandInput;

- (IBAction)click:(id)sender;
- (void) setDefaultFont;
- (void) executeCommand;
- (void) addObject:(NSString*)string;

- (int)numberOfItemsInComboBox:(NSComboBox *)aComboBox;
- (id)comboBox:(NSComboBox *)aComboBox objectValueForItemAtIndex:(int)index;
- (unsigned int)comboBox:(NSComboBox *)aComboBox indexOfItemWithStringValue:(NSString *)string;
//- (NSString *)comboBox:(NSComboBox *)aComboBox completedString:(NSString *)string;


@end
